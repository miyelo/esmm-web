import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  link: {
    margin: theme.spacing(1, 1.5)
  },
  icon: {
    marginRight: theme.spacing(2)
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  cardMedia: {
    paddingTop: "56.25%" // 16:9
  },
  cardContent: {
    flexGrow: 1
  },
  heroContent: {
    position: "relative",
    backgroundImage: "url(https://source.unsplash.com/user/erondu)",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center center",
    backgroundSize: "cover",
    backgroundAttachment: "fixed",
    height: "100%",
    padding: theme.spacing(8, 0, 6)
  },
  heroButtons: {
    marginTop: theme.spacing(4)
  },
  overlay: {
    position: "absolute",
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: "rgba(0,0,0,.3)"
  },
  title: {
    color: "rgba(255, 255, 255, 1)"
  },
  SpecialProduct: {
    position: "relative"
  }
}));

function Home() {
  const [homeData, sethomeData] = useState([]);

  const fetchHome = async () => {
    // const data = await fetch(
    //   `https://fortnite-api.theapinetwork.com/item/get?id=${match.params.id}`,
    //   {
    //     headers: {
    //       Authorization: "7d1d08f3e52997fff36428bc8d3093eb"
    //     }
    //   }
    // );

    //const data = await data.json();

    const data = [
      {
        id: 1,
        tituloNoticia: "Noticia 1",
        extractoNoticia: "Este es un estracto de noticia"
      },
      {
        id: 2,
        tituloNoticia: "Noticia 2",
        extractoNoticia: "Este es un estracto de noticia"
      },
      {
        id: 3,
        tituloNoticia: "Noticia 3",
        extractoNoticia: "Este es un estracto de noticia"
      }
    ];

    console.log(data);
    sethomeData(data);
  };

  useEffect(() => {
    fetchHome();
  }, []);

  const classes = useStyles();

  return (
    <div>
      <div className={classes.heroContent}>
        <div className={classes.overlay} />
        <Container maxWidth="sm" className={classes.SpecialProduct}>
          <Grid container>
            <Grid item md={12}>
              <Typography
                className={classes.title}
                component="h3"
                variant="h3"
                align="center"
                gutterBottom
              >
                Ediciones y Sistemas Médicos de México
              </Typography>
              <Typography
                variant="h5"
                align="center"
                paragraph
                className={classes.title}
              >
                Proveemos experiencia.
              </Typography>
              <div className={classes.heroButtons}>
                <Grid container spacing={2} justify="center">
                  <Grid item>
                    <Button variant="contained">
                      Conocer nuestros productos
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button variant="contained">Agendar una visita</Button>
                  </Grid>
                </Grid>
              </div>
            </Grid>
          </Grid>
        </Container>
      </div>
      <Container className={classes.cardGrid} maxWidth="lg">
        {/* End hero unit */}
        <Grid container spacing={4}>
          {homeData.map(noticia => (
            <Grid item key={noticia.id} xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image="https://source.unsplash.com/random"
                  title="Image title"
                />
                <CardContent className={classes.cardContent}>
                  <Typography gutterBottom variant="h5" component="h2">
                    {noticia.tituloNoticia}
                  </Typography>
                  <Typography>{noticia.extractoNoticia}</Typography>
                </CardContent>
                <CardActions>
                  <Button size="small" variant="outlined" color="primary">
                    Saber más
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  );
}

export default Home;
