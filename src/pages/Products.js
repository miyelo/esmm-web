import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import Paper from "@material-ui/core/Paper";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import {
  getProducts,
  getProduct_async
} from "../apis/firebase/productController";

const useStyles = makeStyles(theme => ({
  cardGrid: {
    paddingBottom: theme.spacing(8)
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  cardMedia: {
    paddingTop: "56.25%" // 16:9
  },
  cardContent: {
    flexGrow: 1
  },
  mainFeaturedPost: {
    position: "relative",
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    marginBottom: theme.spacing(4),
    backgroundImage: "url(https://source.unsplash.com/user/erondu)",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center"
  },
  mainFeaturedPostContent: {
    position: "relative",
    padding: theme.spacing(3),
    [theme.breakpoints.up("md")]: {
      padding: theme.spacing(6),
      paddingRight: 0
    }
  },
  overlay: {
    position: "absolute",
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: "rgba(0,0,0,.3)"
  }
}));

// const cards = [1, 2, 3, 4];

const Products = () => {
  const classes = useStyles();

  const [products, setProducts] = useState([]);

  useEffect(() => {
    const _getProducts = async () => {
      const _products = await getProduct_async();
      setProducts(_products);
    };

    _getProducts();

    // getProducts().on("value", productsSnapshot => {
    //   console.log(productsSnapshot.val());
    //   setProducts(productsSnapshot.val());
    // });
  }, []);

  return (
    <div>
      <Container maxWidth="lg">
        <Paper className={classes.mainFeaturedPost}>
          <div className={classes.overlay} />
          <Grid container>
            <Grid item md={6}>
              <div className={classes.mainFeaturedPostContent}>
                <Typography
                  component="h1"
                  variant="h3"
                  color="inherit"
                  gutterBottom
                >
                  Producto destacado
                </Typography>
                <Typography variant="h5" color="inherit" paragraph>
                  Esto es un producto destacado o con una promoción muy buena.
                </Typography>
                <Link variant="subtitle1" href="#">
                  <Button variant="contained">Ver producto</Button>
                </Link>
              </div>
            </Grid>
          </Grid>
        </Paper>
      </Container>
      <Container className={classes.cardGrid} maxWidth="lg">
        {/* End hero unit */}
        <Grid container spacing={4}>
          {/* {cards.map(card => ( */}

          {/* <FirebaseDatabaseNode
              path="dev/productos/0"
              //limitToFirst={this.state.limit}
              //orderByKey
              //orderByValue={"created_on"}
            > */}
          {products.map(prods => {
            return (
              <Grid item xs={12} sm={6} md={4}>
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image="https://source.unsplash.com/random"
                    title="Image title"
                  />
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      {prods.Nombre}
                    </Typography>
                    <Typography>Detalles de la tarjeta informativa</Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small" variant="outlined" color="primary">
                      Ver
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            );
          })}
          {/* </FirebaseDatabaseNode> */}

          {/* <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image="https://source.unsplash.com/random"
                  title="Image title"
                />
                <CardContent className={classes.cardContent}>
                  <Typography gutterBottom variant="h5" component="h2">
                    Cabecera
                  </Typography>
                  <Typography>Detalles de la tarjeta informativa</Typography>
                </CardContent>
                <CardActions>
                  <Button size="small" variant="outlined" color="primary">
                    Ver
                  </Button>
                </CardActions>
              </Card> */}

          {/* ))} */}
        </Grid>
      </Container>
    </div>
  );
};

export default Products;
