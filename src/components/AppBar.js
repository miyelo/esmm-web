import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Toolbar from "@material-ui/core/Toolbar";
import { makeStyles } from "@material-ui/core/styles";
import { Link as LinkRouter } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  link: {
    margin: theme.spacing(1, 1.5)
  }
}));

const AppBarMine = () => {
  const classes = useStyles();

  return (
    <div>
      <AppBar position="relative" color="default">
        <Toolbar variant="dense">
          <LinkRouter
            to="/"
            variant="button"
            color="textPrimary"
            className={classes.link}
          >
            <Button>ESMM</Button>
          </LinkRouter>

          <nav>
            <LinkRouter
              to="/about"
              variant="button"
              color="textPrimary"
              className={classes.link}
              S
            >
              <Button>Conócenos</Button>
            </LinkRouter>

            <LinkRouter
              variant="button"
              color="textPrimary"
              className={classes.link}
              to="/help"
            >
              <Button>Manuales y Ayuda</Button>
            </LinkRouter>

            <LinkRouter
              variant="button"
              color="textPrimary"
              className={classes.link}
              to="/products"
            >
              <Button>Productos</Button>
            </LinkRouter>
          </nav>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default AppBarMine;
