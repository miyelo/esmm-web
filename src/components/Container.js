import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import ImgMediaCard from './ImgMediaCard';

export default function FixedContainer() {
    return (
        <React.Fragment>
            <CssBaseline />
            <Container fixed ali>
                <Typography component="div" style={{ height: '100vh', }} />
                <ImgMediaCard></ImgMediaCard>
            </Container>
        </React.Fragment>
    );
}