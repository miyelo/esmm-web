import React from "react";
import AppBar from "./components/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import HelpLobby from "./pages/HelpLobby";
import Home from "./pages/Home";
import ProtuctDetails from "./pages/ProtuctDetails";
import Products from "./pages/Products";
import About from "./pages/About";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Footer from "./components/Footer";
import * as firebase from "firebase";
import { firebaseConfig } from "./apis/firebase/config";

firebase.initializeApp(firebaseConfig);

function App() {
  return (
    <div className="App">
      <Router>
        <CssBaseline />
        <AppBar />
        <main>
          <div>
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/about" component={About} />
              <Route path="/help" component={HelpLobby} />
              <Route path="/products" exact component={Products} />
              <Route path="/products/:idProducto" component={ProtuctDetails} />
            </Switch>
          </div>
        </main>
      </Router>
      <Footer />
    </div>
  );
}

export default App;
