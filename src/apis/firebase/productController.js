import * as firebase from "firebase";

export function getProducts() {
  return firebase.database().ref("dev/productos");
}

export function getProduct_async() {
  return firebase.database()
    .ref("dev/productos")
    .once("value")
    .then(productsSnapshot => {
      // mapeo de la info
      console.log(productsSnapshot);
      return productsSnapshot.val();)
}
