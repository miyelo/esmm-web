import { getDate, getProduct_async } from "./productController";
import * as firebase from "firebase";
import { firebaseConfig } from "./config";

// Describe   testSuite = agrupacion de pruebas o supocisiones
// it  prueba o supocision

describe("Firebase products", () => {
  firebase.initializeApp(firebaseConfig);

  it("Me regrese informacion", async () => {
    const products = await getProduct_async();
    expect(products).not.toBeNull();
  });
});
